<?php
$ma1 = [1, 22, 3, 44, 5,];
$x1 = 0;
$ma2 = [66, 7, 88, 9, 10, 11, 12, 13, 14, 95,];
$x2 = 0;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hw6</title>
    <meta name="description" content="Hw6">
    <link rel="stylesheet" href="/styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>

<ul>
    <?php for($t = 0; $t <=4 ; $t ++):?>
    <?php $x1 = $ma1[$t] + $x1?>
    <?php endfor;?>
    <p>Общая сумма через "For": <?= $x1?></p>
</ul>

<ul>
    <?php foreach($ma2 as $ma2summ):?>
        <?php $x2 = $ma2summ + $x2;?>
        <?php endforeach;?>
        <p>Общая сумма через "Foreach": <?= $x2?></p>
</ul>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>